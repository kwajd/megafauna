import { getOrbitControl, setListeners, getRenderer, setContainer, getCamera } from "./common/config.js";
import { manager } from "./LoadingManager.js";

let scene, renderer, camera, container, model, controls;

function init() {
    container = document.querySelector('.scene');
    setContainer(container);
    scene = new THREE.Scene();

    camera = getCamera(-0.5, 0.08, -12, 35, 0.1, 1000);

    //Renderer
    renderer = getRenderer(container);
    container.appendChild(renderer.domElement);

    //Load model
    let loader = new THREE.GLTFLoader(manager);
    loader.load("../model/bird.gltf", function (gltf) {
        scene.add(gltf.scene);
        model = gltf.scene.children[0];
        model.position.set(.5, 1, 0);
        model.rotation.set(1.55, 0, -3.14);
        renderer.render(scene, camera);
    })

    controls = getOrbitControl(camera, renderer, 12, 30, model)
}

function animate() {
    requestAnimationFrame(animate);

    renderer.render(scene, camera);
}

setListeners();
init();
animate();  