var manager = new THREE.LoadingManager(loadModel);

manager.onLoad = function () {
    document.getElementById('loading-screen').remove();
};

manager.onProgress = function (url, itemsLoaded, itemsTotal) {
    let percent = itemsLoaded * 100 / itemsTotal;
    //feedbackData.innerHTML = `Loading ${Math.round(percent)} %`;
};

function loadModel() {
    object.traverse(function (child) {
        if (child.isMesh) child.material.map = texture;
    });

    object.position.y = - 95;
    scene.add(object);

}

export { manager };