import { waterConfig } from "./common/waterConfig.js";
import { sky, parameters, skyConfig } from "./common/skyConfig.js";
import { getOrbitControl, setRenderer, setContainer, getCamera, setResize } from "./common/config.js";
import { manager } from "./LoadingManager.js";

var container;
var camera, scene, renderer, light;
var controls, water, model, bouy, person, sand;

var dol, dol2;

//Count FPS
var t = [];
var fps;

init();
animate();

function init() {

  container = document.querySelector('.scene');
  setContainer(container);

  scene = new THREE.Scene();

  camera = getCamera(30, 30, 100, 10, 1, 1000);

  const ambient = new THREE.AmbientLight(0x404040, 1.3);
  scene.add(ambient);

  light = new THREE.DirectionalLight(0xffffff, 0.7);
  scene.add(light);

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  setRenderer(renderer);
  container.appendChild(renderer.domElement);

  water = waterConfig(light, scene);
  scene.add(water);

  skyConfig(true);

  var cubeCamera = new THREE.CubeCamera(0.1, 1, 512);
  cubeCamera.renderTarget.texture.generateMipmaps = true;
  cubeCamera.renderTarget.texture.minFilter =
    THREE.LinearMipmapLinearFilter;

  scene.background = cubeCamera.renderTarget;

  function updateSun() {
    var theta = Math.PI * (parameters.inclination - 0.5);
    var phi = 2 * Math.PI * (parameters.azimuth - 0.5);

    light.position.x = parameters.distance * Math.cos(phi);
    light.position.y = parameters.distance * Math.sin(phi) * Math.sin(theta);
    light.position.z = parameters.distance * Math.sin(phi) * Math.cos(theta);

    sky.material.uniforms["sunPosition"].value = light.position.copy(
      light.position
    );
    water.material.uniforms["sunDirection"].value
      .copy(light.position)
      .normalize();

    cubeCamera.update(renderer, sky);
  }

  updateSun();

  let GLTFloader = new THREE.GLTFLoader(manager);

  GLTFloader.load("../model/dolphin/dolphinBouy.glb", function (gltf) {
    scene.add(gltf.scene);
    model = gltf.scene.children[1];
    bouy = gltf.scene.children[0];
    model.position.y = 1.28;
    model.rotation.y = -0.002;
    bouy.position.y = 0.28;
    bouy.rotation.z = 0.002;
  });

  GLTFloader.load("../model/dolphin/dolphins.glb", function (gltf) {
    dol = createDolphin(gltf.scene, 0, 0);
    scene.add(dol);
  });

  GLTFloader.load("../model/dolphin/dolphins.glb", function (gltf) {
    dol2 = createDolphin(gltf.scene, -1, 2);
    scene.add(dol2);
  });

  function createDolphin(gltf, left, back) {
    let dol = gltf.children[0];

    dol.position.y = -1.2;
    dol.position.x = 3 + left;
    dol.position.z = 6 + back;

    dol.rotation.x = -0.2;
    dol.rotation.y = 9.5;

    dol.scale.x = 0.35;
    dol.scale.y = 0.35;
    dol.scale.z = 0.35;

    return dol;
  }

  for (let index = -160; index < 150; index += 45) {
    GLTFloader.load("../model/dolphin/palm.glb", function (gltf) {
      createTrees(gltf.scene, index);
    });
  }

  function createTrees(gltf, left) {
    var tree = gltf.children[0];
    tree.scale.x = 0.6;
    tree.scale.y = 0.6;
    tree.scale.z = 0.6;
    tree.position.y = 9;
    tree.position.x = -70;
    tree.position.z = left;
    scene.add(tree);
  }

  for (let index = -150; index < 150; index += 50) {
    GLTFloader.load("../model/dolphinv2/rock.glb", function (gltf) {
      scene.add(gltf.scene);
      if (index % 2 == 0) {
        createRocks(gltf.scene, index, -78);
      } else {
        createRocks(gltf.scene, index);
      }
    });
  }

  function createRocks(gltf, left, back = -80) {
    var rocks = gltf.children[0];
    rocks.position.y = 12;
    rocks.scale.x = 6;
    rocks.scale.y = 4;
    rocks.scale.z = 4;
    rocks.rotation.z = 1.6;
    rocks.position.x = back;
    rocks.position.z = left;

    scene.add(rocks);
  }

  GLTFloader.load("../model/dolphin/sand.glb", function (gltf) {
    scene.add(gltf.scene);
    sand = gltf.scene.children[0];
    sand.rotation.z = -1.56;
    sand.position.y = 8;
    sand.position.x = -70;
    sand.scale.x = 300;
    sand.scale.z = 20;
    sand.scale.y = 20;
  });

  GLTFloader.load("../model/dolphinv2/person/scene.gltf", function (gltf) {
    person = gltf.scene.children[0];
    person.position.set(.5, 0, 4.5);
    person.rotation.set(177.6, 179, 0);
    scene.add(gltf.scene);
  });

  renderer.render(scene, camera);

  controls = getOrbitControl(camera, renderer, 30, 100, model)

  setResize()
}



function animate(now) {
  t.unshift(now);
  if (t.length > 10) {
    var t0 = t.pop();
    fps = Math.floor((1000 * 10) / (now - t0));
  }
  requestAnimationFrame(animate);


  render();
}

function render() {

  var time = performance.now() * 0.001;
  if (model && bouy && person && dol && dol2) {
    model.position.y = Math.sin(time) * 0.07 + 1.25;
    bouy.position.y = Math.sin(time) * 0.07 + 0.25;
    person.position.y = Math.sin(time) * 0.07 + 1.1;
    dol.position.y = Math.sin(time) * 0.12 - 1.25;
    dol2.position.y = Math.sin(time) * 0.12 - 1.25;
  }


  if (fps <= 65) {
    water.material.uniforms["time"].value += 0.5 / 60.0;
  } else {
    water.material.uniforms["time"].value += 0.2 / 60.0;
  }

  renderer.render(scene, camera);
}
