import * as THREEModule from "../three.module.js";
import { waterConfig } from "./common/waterConfig.js";
import { sky, parameters, skyConfig } from "./common/skyConfig.js";
import { manager } from "./LoadingManager.js";
import { OBJLoader } from "../OBJLoader.js";
import { MTLLoader } from "../MTLLoader.js";
;
var camera, scene, renderer, light;
var controls, water, model, person, person2;
var turtle;

init();
animate();

function init() {
  renderer = new THREEModule.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  scene = new THREEModule.Scene();

  camera = new THREE.PerspectiveCamera(
    10,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );

  camera.position.set(-56.32, 9.72, -30.33);

  light = new THREEModule.DirectionalLight(0xffffff, .9);
  scene.add(light);

  water = waterConfig(light, scene);
  scene.add(water);

  skyConfig(true);

  var cubeCamera = new THREEModule.CubeCamera(0.1, 1, 512);
  cubeCamera.renderTarget.texture.generateMipmaps = true;
  cubeCamera.renderTarget.texture.minFilter =
    THREEModule.LinearMipmapLinearFilter;

  scene.background = cubeCamera.renderTarget;

  function updateSun() {
    var theta = Math.PI * (parameters.inclination - 0.5);
    var phi = 2 * Math.PI * (parameters.azimuth - 0.5);

    light.position.x = parameters.distance * Math.cos(phi);
    light.position.y = parameters.distance * Math.sin(phi) * Math.sin(theta);
    light.position.z = parameters.distance * Math.sin(phi) * Math.cos(theta);

    sky.material.uniforms["sunPosition"].value = light.position.copy(
      light.position
    );
    water.material.uniforms["sunDirection"].value
      .copy(light.position)
      .normalize();

    cubeCamera.update(renderer, sky);
  }

  updateSun();

  let loader = new THREE.GLTFLoader(manager);
  loader.load("../model/turtle.gltf", function (gltf) {
    scene.add(gltf.scene);


    model = gltf.scene.children[1];
    model.position.set(0, 6.6, 0);
    model.rotation.set(3, 0, -3.14);
  });


  loader.load("../model/turtle/turtle.glb", function onLoad(glb) {
    scene.add(glb.scene);
    turtle = glb.scene.children[1];
    turtle.position.set(0, 5, 0);
    turtle.rotation.set(1.3, 0, 0);
    turtle.scale.x = -0.1;
    turtle.scale.y = -0.1;
    turtle.scale.z = -0.1;
  })


  var sand;
  loader.load("../model/dolphin/sand.glb", function (gltf) {
    scene.add(gltf.scene);
    sand = gltf.scene.children[0];
    sand.rotation.z = -1.9;
    sand.position.y = 3;
    sand.position.x = 30;
    sand.scale.x = 500;
    sand.scale.z = 20;
    sand.scale.y = 200;

  });

  let OBJloader = new MTLLoader();
  OBJloader.load("../model/dolphin/person/person.mtl", function (materials) {
    materials.preload();
    new OBJLoader()
      .setMaterials(materials)
      .load("../model/dolphin/person/person.obj", function (object) {
        object.rotation.y = 5;
        object.position.y = 0.48;
        object.position.z = -0.8;
        object.position.x = -2.1;
        object.scale.x = 0.55;
        object.scale.y = 0.55;
        object.scale.z = 0.55;

        var texture = new THREE.TextureLoader().load(
          "../model/dolphin/person/textures"
        );

        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material.map = texture;
          }
        });

        person = object;
        scene.add(object);
      });
  });

  OBJloader.load("../model/turtle/person2.mtl", function (materials) {
    materials.preload();
    new OBJLoader()
      .setMaterials(materials)
      .load("../model/turtle/person2.obj", function (object) {
        object.rotation.y = 0;
        object.position.y = 0.48;
        object.position.z = 0.2;
        object.position.x = -0.7;
        object.scale.x = 0.55;
        object.scale.y = 0.55;
        object.scale.z = 0.55;

        var texture = new THREE.TextureLoader().load(
          "../model/turtle/textures"
        );

        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material.map = texture;
          }
        });

        person2 = object;
        scene.add(object);
      });
  });

  renderer.render(scene, camera);

  controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.maxPolarAngle = Math.PI * 0.495;
  controls.minDistance = 40.0;
  controls.maxDistance = 200.0;
  controls.update();

  window.addEventListener("resize", onWindowResize, false);
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
  requestAnimationFrame(animate);

  render();

}

function render() {
  if (model != undefined) {
    model.position.y = 5;
    controls.target.set(model.position.x, model.position.y, model.position.z);
    controls.update();

  }

  water.material.uniforms['time'].value += .3 / 60.0;
  renderer.render(scene, camera);
}
