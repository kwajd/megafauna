var camera, controls, renderer, container;
var prevDeltaX = 0, prevDeltaY = 0;

function getCamera(x, y, z, fov = 35, near = 1, far = 1000) {
    camera = new THREE.PerspectiveCamera(fov, container.clientWidth / container.clientHeight, near, far);
    camera.position.set(x, y, z);

    return camera;
}

function setContainer(aContainer) {
    container = aContainer;
}

function setRenderer(aRenderer) {
    renderer = aRenderer;
}

function getOrbitControl(camera, renderer, min, max, model) {
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.maxPolarAngle = Math.PI / 2;
    controls.minDistance = min;
    controls.maxDistance = max;
    model && controls.target.set(model.position.x, model.position.y, model.position.z);
    controls.update();

    return controls;
}

function onWindowResize(camera, container, renderer) {
    camera.aspect = container.clientWidth / container.clientHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(container.clientWidth, container.clientHeight);
}

function onDocumentMouseMove(event, container, controls) {
    let mouseX = event.clientX;
    let mouseY = event.clientY;
    let deltaX = (container.clientWidth / 2 - mouseX);
    let deltaY = (mouseY - container.clientHeight / 2);

    controls.rotateHorizontal((deltaX - prevDeltaX) * 0.08 * Math.PI / 180)
    controls.rotateVertical((deltaY - prevDeltaY) * 0.05 * Math.PI / 180)

    prevDeltaX = deltaX; prevDeltaY = deltaY;
}

function getRenderer(container) {
    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(container.clientWidth, container.clientHeight);
    renderer.setPixelRatio(window.devicePixelRatio);

    return renderer;
}

function handleMovement(event) {
    onDocumentMouseMove(event, container, controls)
}

function setListeners() {
    ['mousemove', 'touchmove'].forEach(function (evt) {
        document.addEventListener(evt, handleMovement);
    });

    window.addEventListener("resize", () => onWindowResize(camera, container, renderer));
}

function setResize() {
    window.addEventListener("resize", () => onWindowResize(camera, container, renderer));
}


export { setResize, getCamera, getOrbitControl, onDocumentMouseMove, getRenderer, setListeners, setContainer, setRenderer };