import * as THREEModule from "../../three.module.js";
import { Water } from "../../objects/Water.js";

function waterConfig(light, scene) {

    var waterGeometry = new THREEModule.PlaneBufferGeometry(10000, 10000);

    var water = new Water(waterGeometry, {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: new THREEModule.TextureLoader().load(
            "../../textures/waternormals.jpg",
            function (texture) {
                texture.wrapS = texture.wrapT = THREEModule.RepeatWrapping;
            }
        ),
        alpha: 1.0,
        sunDirection: light.position.clone().normalize(),
        sunColor: 0xffffff,
        waterColor: 0x001e0f,
        distortionScale: 3.7,
        fog: scene.fog !== undefined,
    });

    water.rotation.x = -Math.PI / 2;

    return water;

}

export { waterConfig };