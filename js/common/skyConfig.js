import { Sky } from "../../objects/Sky.js";

var sky, uniforms, parameters

var dayConfig = [{
    distance: 200,
    inclination: 0.2,
    azimuth: 0.1,
    sun: true
}, {
    rayleigh: 0.165,
}];

var nightConfig = [{
    distance: 200,
    inclination: 0.512,
    azimuth: 0.1,
}, {
    rayleigh: 3,
}];

function skyConfig(day) {
    sky = new Sky();

    uniforms = sky.material.uniforms;

    uniforms['turbidity'].value = 3;
    uniforms['rayleigh'].value = day ? dayConfig[1].rayleigh : nightConfig[1].rayleigh;
    uniforms['luminance'].value = .8;
    uniforms["mieCoefficient"].value = 0.005;
    uniforms["mieDirectionalG"].value = 0.963;



    if (day)
        parameters = dayConfig[0];
    else
        parameters = nightConfig[0];
}



export { skyConfig, sky, uniforms, parameters };