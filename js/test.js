import * as THREEModule from "../three.module.js";
import { getOrbitControl, setRenderer, setContainer, getCamera } from "./common/config.js";
import { waterConfig } from "./common/waterConfig.js";
import { sky, parameters, skyConfig } from "./common/skyConfig.js";
import { manager } from "./LoadingManager.js";

var container;
var camera, scene, water, renderer, light;
var controls, model, bouy, terrain, island;
var prevDeltaX = 0, prevDeltaY = 0;

init();
animate();

function init() {


    container = document.querySelector(".scene");
    setContainer(container);
    scene = new THREE.Scene();

    camera = getCamera(30, 30, 100, 10, 1, 1000);

    renderer = new THREEModule.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    setRenderer(renderer);
    container.appendChild(renderer.domElement);

    const ambient = new THREE.AmbientLight(0xffffff, 0.3);
    scene.add(ambient);

    light = new THREEModule.DirectionalLight(0xffffff, 0.8);
    scene.add(light);

    water = waterConfig(light, scene);
    scene.add(water);

    skyConfig(true);

    var cubeCamera = new THREEModule.CubeCamera(0.1, 1, 512);
    cubeCamera.renderTarget.texture.generateMipmaps = true;
    cubeCamera.renderTarget.texture.minFilter =
        THREEModule.LinearMipmapLinearFilter;

    scene.background = cubeCamera.renderTarget;

    function updateSun() {
        var theta = Math.PI * (parameters.inclination - 0.5);
        var phi = 2 * Math.PI * (parameters.azimuth - 0.5);

        light.position.x = parameters.distance * Math.cos(phi);
        light.position.y = parameters.distance * Math.sin(phi) * Math.sin(theta);
        light.position.z = parameters.distance * Math.sin(phi) * Math.cos(theta);

        sky.material.uniforms['sunPosition'].value = light.position.copy(light.position);
        water.material.uniforms['sunDirection'].value.copy(light.position).normalize();

        cubeCamera.update(renderer, sky);
    }

    updateSun();
    manager.addHandler(/\.tga$/i, new THREE.TGALoader());
    let GLTFloader = new THREE.GLTFLoader(manager);
    let FBXloader = new THREE.FBXLoader(manager);

    GLTFloader.load("../model/dolphin/dolphinBouy.glb", function (gltf) {
        scene.add(gltf.scene);
        model = gltf.scene.children[1];
        bouy = gltf.scene.children[0];
        model.position.y = 1.28;
        model.rotation.y = -0.002;
        bouy.position.y = 0.28;
        bouy.rotation.z = 0.002;
    });

    GLTFloader.load("../model/test/island.glb", function (gltf) {
        scene.add(gltf.scene);
        console.log(gltf);
        island = gltf.scene.children[0];
        island.position.set(-65, 3.1, 50);
        island.scale.set(2, 2, 2)
    });

    /*GLTFloader.load("../model/Terrain.glb", function (gltf) {
        scene.add(gltf.scene);
        terrain = gltf.scene.children[0];
        terrain.position.set(350, -1.2, -500);
        terrain.scale.set(.7, 0.7, 0.7)
    });

    GLTFloader.load("../model/Island.glb", function (gltf) {
        scene.add(gltf.scene);
        island = gltf.scene.children[0];
        island.position.set(0, 2, 0);
        island.scale.set(8, 8, 8)
    });


    FBXloader.load("../model/test/Island3.fbx", function (object) {
        console.log(object.children);
        scene.add(object);

        object.children.forEach(element => {
            element.position.set(20, 0, -170)
            element.scale.set(.01, .01, .01)
            element.rotation.set(0, 170, 0)
        });

    });*/

    renderer.render(scene, camera);

    controls = getOrbitControl(camera, renderer, 10, 1000, model)

    window.addEventListener("resize", onWindowResize, false);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

init();
animate();

function animate() {
    requestAnimationFrame(animate);
    controls.update();

    render();
}

function render() {
    var time = performance.now() * 0.001;
    model.position.y = Math.sin(time) * 0.13 + 1.28;
    bouy.position.y = Math.sin(time) * 0.13 + 0.28;
    controls.target.set(model.position.x, model.position.y, model.position.z);

    water.material.uniforms['time'].value += .3 / 60.0;

    renderer.render(scene, camera);
}
