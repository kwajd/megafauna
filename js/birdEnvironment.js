import * as THREEModule from "../three.module.js";
import { getOrbitControl, setRenderer, setContainer, getCamera } from "./common/config.js";
import { waterConfig } from "./common/waterConfig.js";
import { sky, parameters, skyConfig } from "./common/skyConfig.js";
import { manager } from "./LoadingManager.js";

var camera, scene, renderer, light, container;
var controls, water
var stop_animation = false;
var bird_model_dom;
var bird_model_bouys;
var bird1;
var bird2;
var bird3;
var bird_flying;
var boat_model
var pessoa;
//Count FPS
var t = [];
var fps;



function init() {
  container = document.querySelector('.scene');
  setContainer(container);
  scene = new THREE.Scene();

  camera = getCamera(30, 30, 100, 10, 1, 1000);

  const ambient = new THREE.AmbientLight(0x404040, 1)
  scene.add(ambient)

  light = new THREE.DirectionalLight(0xffffff, .9);
  light.position.set(10, 10, 10)
  scene.add(light);

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  setRenderer(renderer);
  container.appendChild(renderer.domElement);

  water = waterConfig(light, scene);
  scene.add(water);

  skyConfig(true);

  var cubeCamera = new THREE.CubeCamera(0.1, 100, 512);
  cubeCamera.renderTarget.texture.generateMipmaps = true;
  cubeCamera.renderTarget.texture.minFilter =
    THREE.LinearMipmapLinearFilter;

  scene.background = cubeCamera.renderTarget;

  function updateSun() {
    var theta = Math.PI * (parameters.inclination - 0.5);
    var phi = 2 * Math.PI * (parameters.azimuth - 0.5);

    light.position.x = parameters.distance * Math.cos(phi);
    light.position.y = parameters.distance * Math.sin(phi) * Math.sin(theta);
    light.position.z = parameters.distance * Math.sin(phi) * Math.cos(theta);

    sky.material.uniforms["sunPosition"].value = light.position.copy(
      light.position
    );
    water.material.uniforms["sunDirection"].value
      .copy(light.position)
      .normalize();

    cubeCamera.update(renderer, sky);
  }

  updateSun();

  let loader = new THREE.GLTFLoader(manager);
  loader.load("../model/bird_assets/DOME_BIRD_BOAIS.glb", function onLoad(gltf) {
    scene.add(gltf.scene);
    bird_model_bouys = gltf.scene.children;
    bird_model_dom = gltf.scene.children[10];
    bird1 = gltf.scene.children[11];
    bird2 = gltf.scene.children[12];
    bird3 = gltf.scene.children[13];

    //Bird 1
    bird1.scale.set(2, 2, 2);
    bird1.position.z += 1;
    bird1.position.x += 0.05;
    bird1.rotation.set(0, 1, 0)

    //Bird 2  
    bird2.scale.set(2, 2, 2);
    bird2.position.z += 0.1;
    bird2.position.x -= 0.1;

    //Bird 3
    bird3.scale.set(2, 2, 2);
    bird3.position.z += 0.15;
    bird3.rotation.set(0, 1, 0)

    // Dome
    bird_model_dom.position.set(-0.5, 3.5, 0);
    bird_model_dom.rotation.set(1.55, 0, 0);
  })

  loader.load("../model/bird_assets/BARCO.glb", function onLoad(glb) {
    scene.add(glb.scene);
    boat_model = glb.scene.children;
    boat_model.forEach(element => {
      element.position.y = element.position.y + 2.4;
      element.position.x = element.position.x + 6;
    });
  })

  //load bird
  loader.load("../model/bird_assets/seagull_v1/seagull.gltf", function onLoad(glb) {
    scene.add(glb.scene);
    bird_flying = glb.scene.children[0];
    bird_flying.position.set(-5, 6, 0);
    bird_flying.rotation.set(-1.9, 0, 3);
    bird_flying.scale.set(0.1, 0.1, 0.1);
  })

  //load people
  loader.load("../model/bird_assets/character/pessoa.gltf", function onLoad(gltf) {
    scene.add(gltf.scene);
    pessoa = gltf.scene.children[0];
    pessoa.position.set(14.3, 1, -3);
    pessoa.rotation.set(-1.55, 0, 3)
    pessoa.scale.set(0.06, 0.06, 0.06);
  })

  renderer.render(scene, camera);

  controls = getOrbitControl(camera, renderer, 10, 150, bird_model_dom)

  window.addEventListener("resize", onWindowResize, false);
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

init();
animate();


function animate(now) {
  t.unshift(now);
  if (t.length > 10) {
    var t0 = t.pop();
    fps = Math.floor((1000 * 10) / (now - t0));
  }
  requestAnimationFrame(animate);
  render();

}

function render() {
  if (bird_model_dom != undefined) {
    var time = performance.now() * 0.001;
    bird_model_bouys.forEach((element, index) => {
      if (index < 10) {
        element.position.y = Math.sin(time) * 0.1 + 0;
      }
    });
    bird_model_dom.position.y = Math.sin(time) * 0.1 + 3;
    bird1.position.y = Math.sin(time) * 0.1 + 3.8;
    bird2.position.y = Math.sin(time) * 0.1 + 2.3;
    bird3.position.y = Math.sin(time) * 0.1 + 2.85;

  }

  water.material.uniforms["time"].value += 0.5 / 60.0;

  renderer.render(scene, camera);
}
