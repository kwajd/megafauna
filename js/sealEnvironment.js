let scene, renderer, camera, container, model, composer, sealModel;
import { manager } from "./LoadingManager.js";

function init() {


    //scene
    container = document.querySelector('.scene');
    scene = new THREE.Scene();

    //Renderer
    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0x000000, 1);
    document.body.appendChild(renderer.domElement);

    //Camera Setup
    camera = new THREE.PerspectiveCamera(50, window.clientWidth / window.clientHeight, 0.01, 5);
    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enableZoom = true;
    controls.enableKeys = true;
    controls.screenSpacePanning = false;
    controls.maxPolarAngle = 1.2;
    controls.maxDistance = 1;
    controls.target = new THREE.Vector3(0.150, -0.250, 0.060); //targets the seal
    camera.position.set(0.3, -0.054196, -0.39100867);
    camera.rotation.set(-2.732000, 0.427731, 2.9634);

    //controls for orbit controls


    //domes
    //Load ocean floor
    let loader = new THREE.GLTFLoader(manager);
    loader.load("../model/scene.gltf", function (gltf) {
        scene.add(gltf.scene);
        model = gltf.scene.children[0];
        //model.position.set(0, 0, 0); //If we don't distance from center then it gets blocked by background
        animate();
    });

    //Load dome
    let loaderSeal = new THREE.GLTFLoader();
    loaderSeal.load("../model/seal.gltf", function (gltf) {
        scene.add(gltf.scene);
        sealModel = gltf.scene.children[0];
        sealModel.position.set(0.150, -0.250, 0.060);
        sealModel.rotation.set(0, -1.840, 0);
        sealModel.scale.set(0.04, 0.04, 0.04);
        animate();
    });


    //lights
    const ambient = new THREE.AmbientLight(0x0a4c4b, 1);
    scene.add(ambient);

    const light = new THREE.DirectionalLight(0xffffff, 0.2);
    light.position.set(500, 500, 500);
    //scene.add(light);

    const light2 = new THREE.DirectionalLight(0x0a4c4b, 0.5);
    light2.position.set(0, 500, -500);
    scene.add(light2);

    var spotLight = new THREE.SpotLight(0x138a88, 0.4, 90, 0.11, 1, 2);
    spotLight.position.set(1.5, 1, 1);
    spotLight.castShadow = true;
    spotLight.shadow.mapSize.width = 1024;
    spotLight.shadow.mapSize.height = 1024;
    spotLight.shadow.camera.near = 500;
    spotLight.shadow.camera.far = 4000;
    spotLight.shadow.camera.fov = 30;
    spotLight.target.position.set(0.150, -0.250, 0.060);
    scene.add(spotLight);
    scene.add(spotLight.target);

    var spotLight2 = new THREE.SpotLight(0x138a88, 1, 90, 0.11, 1, 2);
    spotLight2.position.set(-1.5, 1, 1);
    spotLight2.castShadow = true;
    spotLight2.shadow.mapSize.width = 1024;
    spotLight2.shadow.mapSize.height = 1024;
    spotLight2.shadow.camera.near = 500;
    spotLight2.shadow.camera.far = 4000;
    spotLight2.shadow.camera.fov = 30;
    spotLight2.target.position.set(0.150, -0.250, 0.060);
    scene.add(spotLight2);
    scene.add(spotLight2.target);

    var spotLight3 = new THREE.SpotLight(0x138a88, 0.4, 90, 0.11, 1, 2);
    spotLight3.position.set(-1.5, 1, 1);
    spotLight3.castShadow = true;
    spotLight3.shadow.mapSize.width = 1024;
    spotLight3.shadow.mapSize.height = 1024;
    spotLight3.shadow.camera.near = 500;
    spotLight3.shadow.camera.far = 4000;
    spotLight3.shadow.camera.fov = 30;
    spotLight3.target.position.set(0.150, -0.250, 0.060);
    scene.add(spotLight3);
    scene.add(spotLight3.target);

    var spotLight4 = new THREE.SpotLight(0x138a88, 1, 90, 0.11, 1, 2);
    spotLight4.position.set(-1.5, 1, -1);
    spotLight4.castShadow = true;
    spotLight4.shadow.mapSize.width = 1024;
    spotLight4.shadow.mapSize.height = 1024;
    spotLight4.shadow.camera.near = 500;
    spotLight4.shadow.camera.far = 4000;
    spotLight4.shadow.camera.fov = 30;
    spotLight4.target.position.set(0.150, -0.250, 0.060);
    scene.add(spotLight4);
    scene.add(spotLight4.target);

    //processing
    /*composer = new POSTPROCESSING.EffectComposer(renderer);
    composer.addPass(new POSTPROCESSING.RenderPass(scene, camera));
    const effectPass = new POSTPROCESSING.EffectPass(
        camera, 
        new POSTPROCESSING,BloomEffect()
    );
    effectPass.renderToScreen = true;
    composer.addPass(effectPass);*/


}

function animate() {
    requestAnimationFrame(animate);
    //camera.lookAt(scene.position);

    renderer.render(scene, camera);


    /*console.log("position:");
    console.log(camera.position);
    console.log("rotation:");
    console.log(camera.rotation);
    console.log("-----------------");*/
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener("resize", onWindowResize);


init();
animate();  