import { getOrbitControl, setListeners, getRenderer, setContainer, getCamera } from "./common/config.js";
import { manager } from "./LoadingManager.js";

let scene, renderer, camera, container, model, controls;

function init() {
    container = document.querySelector('.scene');
    setContainer(container);
    scene = new THREE.Scene();

    //Camera Setup
    camera = getCamera(0, 1, 12, 35, 0.1, 1000);

    const ambient = new THREE.AmbientLight(0x404040, 0);
    scene.add(ambient);

    const light = new THREE.DirectionalLight(0xffffff, 0);
    light.position.set(0, 500, 0);
    scene.add(light);

    //Renderer
    renderer = getRenderer(container);
    container.appendChild(renderer.domElement);

    //Load Model
    let loader = new THREE.GLTFLoader(manager);
    loader.load("../model/whale.gltf", function (gltf) {
        scene.add(gltf.scene);
        model = gltf.scene.children[0];
        model.position.set(0, 0, 0);
        model.rotation.set(1.59, 0, -5);
        renderer.render(scene, camera);
    })

    controls = getOrbitControl(camera, renderer, 12, 30, model)
}

function animate() {
    requestAnimationFrame(animate);

    renderer.render(scene, camera);
}

setListeners();
init();
animate();  